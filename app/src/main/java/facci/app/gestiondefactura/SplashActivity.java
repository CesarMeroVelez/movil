package facci.app.gestiondefactura;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import facci.app.gestiondefactura.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

//esto para los segundos en pantalla del splash
    static int TIMEOUT_MILLIS = 8000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, TIMEOUT_MILLIS);
    }
}
